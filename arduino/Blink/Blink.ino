#include <SoftwareSerial.h>
SoftwareSerial serial(3, 4);
void setup() {
  pinMode(4, OUTPUT);
  pinMode(2, OUTPUT);
  serial.begin(115200);
  serial.println("Hello World!");
}

void loop() {     
  digitalWrite(2, HIGH);   
  delay(1000);             
  digitalWrite(2, LOW);    
 
  digitalWrite(4, HIGH);   
  delay(1000);             
  digitalWrite(4, LOW);     
}

#define AUD PB3
#define LED PB4
#define THRESH 25 

int signal;

void setup() {
  pinMode(AUD, INPUT);
  pinMode(LED, OUTPUT);
}

void loop() {
  signal = analogRead(AUD);
  if(signal > THRESH){
   digitalWrite(LED, HIGH); 
  }else{
   digitalWrite(LED, LOW);
  }
}

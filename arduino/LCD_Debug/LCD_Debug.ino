#include "DEV_Config.h"
#include "LCD_Driver.h"
#include "LCD_GUI.h"

extern LCD_DIS sLCD_DIS;

const int droplength = 150;
COLOR potColor = 0x0000;
COLOR butColor = 0xFFFF;
COLOR audColor = 0x0000;
int pot = A4;
int but = PD2;
int aud = A0;
int potRead = 0;
int audRead = 0;

void setup()
{
  pinMode(but, INPUT);
  pinMode(aud, INPUT);
  pinMode(pot, INPUT);
  System_Init();
  LCD_SCAN_DIR Lcd_ScanDir = SCAN_DIR_DFT;  
  LCD_Init( Lcd_ScanDir, 100);
  LCD_Clear(BLACK);
  GUI_DisString_EN(10, 10, "Button", &Font20, BLACK, WHITE);
  GUI_DisString_EN(10, 100, "AudioIn", &Font20, BLACK, WHITE);
  GUI_DisString_EN(10, 150, "Potentiometer", &Font20, BLACK, WHITE);
}

int concatenate(int x, int y) {
    unsigned pow = 10;
    while(y >= pow)
        pow *= 10;
    return x * pow + y;        
}

int calcColor(int potVal){

  int redVal = 0;
  int grnVal = 0 ;
  int bluVal= 0;
  COLOR col = 0x0000;
  if (potVal < 341)  // Lowest third of the potentiometer's range (0-340)
  {                  
    potVal = (potVal * 3) / 4; // Normalize to 0-255

    redVal = 256 - potVal;  // Red from full to off
    grnVal = potVal;        // Green from off to full
    bluVal = 1;             // Blue off
  }
  else if (potVal < 682) // Middle third of potentiometer's range (341-681)
  {
    potVal = ( (potVal-341) * 3) / 4; // Normalize to 0-255

    redVal = 1;            // Red off
    grnVal = 256 - potVal; // Green from full to off
    bluVal = potVal;       // Blue from off to full
  }
  else  // Upper third of potentiometer"s range (682-1023)
  {
    potVal = ( (potVal-683) * 3) / 4; // Normalize to 0-255

    redVal = potVal;       // Red from off to full
    grnVal = 1;            // Green off
    bluVal = 256 - potVal; // Blue from full to off
  }
  col = concatenate(redVal, grnVal);
  col = concatenate(col, bluVal);
  col = concatenate(col, 0x0);
  return col;
  }

void loop()
{
  //Button
  LCD_SetArealColor(10, 40, 90, 90, butColor);
  if(digitalRead(but) == HIGH){
    butColor = random(1000, 65000);
  }

  //Audio
  audRead = analogRead(aud);
  GUI_DisNum(10, 130, audRead, &Font16, BLACK, WHITE);
  
  //Potentiometer
  potRead = analogRead(pot); 
  potColor = calcColor(potRead);
  LCD_SetArealColor(10, 180, 90, 230, potColor);
  GUI_DisNum(60, 240, potRead, &Font16, BLACK, WHITE);
}

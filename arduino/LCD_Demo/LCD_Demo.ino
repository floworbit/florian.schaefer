#include "DEV_Config.h"
#include "LCD_Driver.h"
#include "LCD_GUI.h"

extern LCD_DIS sLCD_DIS;
int count;
COLOR currentColor;

void drawSmiley(POINT X, POINT Y)
{
  GUI_DrawCircle(X, Y, 80, YELLOW, DRAW_EMPTY, DOT_PIXEL_DFT);
  GUI_DrawCircle(X-30, Y-30, 15, WHITE, DRAW_FULL, DOT_PIXEL_DFT);
  GUI_DrawCircle(X+30, Y-30, 15, WHITE, DRAW_FULL, DOT_PIXEL_DFT);
  GUI_DrawCircle(X-30, Y-30, 5, BLACK, DRAW_FULL, DOT_PIXEL_DFT);
  GUI_DrawCircle(X+30, Y-30, 5, BLACK, DRAW_FULL, DOT_PIXEL_DFT);
  int mundHorizontal = X-45;
  int mundVertikal = Y+30;
  for(float i = 3.141; i < 6.282; i = i + 0.01){
    GUI_DrawCircle(mundHorizontal + i * 10, mundVertikal + sin(-i)*20, 5, RED, DRAW_EMPTY, DOT_PIXEL_DFT);  
    }
}

void sparkle()
{
//LCD_SetPointlColor( POINT Xpoint, POINT Ypoint, COLOR Color);
}

void setup()
{
  System_Init();
  LCD_SCAN_DIR Lcd_ScanDir = SCAN_DIR_DFT;  
  LCD_Init( Lcd_ScanDir, 100);
  GUI_Clear(BLACK);
  GUI_DisString_EN(15, 30, "Optic neuritis Video Synthesizer", &Font20, LCD_BACKGROUND, GREEN);
  drawSmiley(230, 170);
}

void loop()
{
}

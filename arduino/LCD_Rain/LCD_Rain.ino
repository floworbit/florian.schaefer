#include "DEV_Config.h"
#include "LCD_Driver.h"
#include "LCD_GUI.h"

extern LCD_DIS sLCD_DIS;

const int droplength = 150;
COLOR raincolor = RED;
int but = PD2;
int x = 0;
int y = 0;

void setup()
{
  pinMode(but, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(but), press, LOW);
  System_Init();
  LCD_SCAN_DIR Lcd_ScanDir = SCAN_DIR_DFT;  
  LCD_Init( Lcd_ScanDir, 100);
  LCD_Clear(BLACK);
  
}

void raindropV(POINT x)
{
  for(int i = 0; i < sLCD_DIS.LCD_Dis_Page + droplength; i++)
  {
    LCD_SetPointlColor(x, i, raincolor);
    LCD_SetPointlColor(x+1, i, raincolor);
    LCD_SetPointlColor(x, i-droplength, raincolor+1000);
    LCD_SetPointlColor(x+1, i-droplength, raincolor+1000);
  }
}


void raindropH(POINT y)
{
  for(int i = 0; i < sLCD_DIS.LCD_Dis_Column + droplength; i++)
  {
    LCD_SetPointlColor(i, y, raincolor);
    LCD_SetPointlColor(i, y+1, raincolor);
    LCD_SetPointlColor(i-droplength, y, raincolor+1000);
    LCD_SetPointlColor(i-droplength, y+1, raincolor+1000);
  }
}

void press() {
  raincolor = random(1000, 65000);
}

void loop()
{
  raindropV(random(sLCD_DIS.LCD_Dis_Column));
  raindropH(random(sLCD_DIS.LCD_Dis_Page));
}

#include <SoftwareSerial.h>

#define LED1 4
#define LED2 2
#define BTN 3

uint8_t blinks = 0;

SoftwareSerial serial(1, 0); // RX, TX

void btnCount() {
  int pressTimeout = 20000;
  while(pressTimeout > 0){
    if(digitalRead(BTN) == HIGH){
      blinks++;
      serial.print("Will blink ");
      serial.print(blinks);
      serial.println(" times.");
      pressTimeout = 10000;
     }
     delay(1);
     pressTimeout--;
    }
    serial.println("Done getting input.");
    delay(2000);
 }
  
int blinkTimes() {
  for(int i = 0; i < blinks; i++ ){
    if(i%3 == 0){ // Use a different LED every third time to have some variety.
      blink(LED1);  
    }else{
      blink(LED2);
    }
  }
}

void blink(uint8_t pin){
  digitalWrite(pin, HIGH);   
  delay(1000);             
  digitalWrite(pin, LOW);
  delay(1000);             
}

void setup() {  
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(BTN, INPUT);
  serial.begin(2400);
  
  serial.println("Initialized");
}

void loop(){
  serial.println("Please input the number of blinks by pressing the magic button.");

  btnCount();
  blinkTimes();
 }

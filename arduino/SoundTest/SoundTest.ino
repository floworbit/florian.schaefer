#define AUD A1
#define LED 4

int t;
int sound;

void setup() {
  pinMode(AUD, OUTPUT);
  pinMode(LED, OUTPUT);
  digitalWrite(LED, HIGH); 
  t = 0;
}

void loop() {
  t++;
  
  //sound = ((-t&4095)*(255&t*(t&t>>13))>>12)+(127&t*(234&t>>8&t>>3)>>(3&t>>14)); //7-tejeez bad
  //sound = t*(t>>((t>>9|t>>8))&63&t>>4); //8-visy decent
  sound = (t>>6|t|t>>(t>>16))*10+((t>>11)&7); //9-viznut good
  //sound = (t>>7|t|t>>6)*10+4*(t&t>>13|t>>6); //d-viznut-xpansive-varjohukka good
  analogWrite(AUD,sound%255);
}
